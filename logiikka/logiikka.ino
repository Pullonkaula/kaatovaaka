// Erkka Virtanen 20.11.2019, Pullonkaulan käyttöön

#include <vector>
#include <string>
#include <utility>
#include <HX711.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

//************* General parameter definitions ********************/
#define BAUDRATE 115200

//**************** Nodemcu pin definitions ***********************/
#define CLK D0
int SDA_PINS[] = {D1, D2, D3, D4}; // Serial data pins for HX711 modules, in order.

//**************** scale properties ******************************/
int n_scales = sizeof(SDA_PINS)/sizeof(SDA_PINS[0]);
int CAL_ARRAY[] = {2139, 2139, 2139, 2139}; //These values are obtained using the calibraattori sketch
HX711 SCALE_ARRAY[] = {};

//**************** Wifi properties *******************************/
const char* wifi_name = "Mainospaikka_myynnissa";
const char* wifi_password = "pull0nk4ul4";
  
//**************** udp properties ********************************/
u_int input_port = 666;
char incoming_request[255];
WiFiUDP udp;

//*************** Main code **************************************/
std::vector<std::string> udp_command;

// Connects to network and prints info about progress on serial
void connect_to_network(){
  WiFi.begin(wifi_name, wifi_password);
  
  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED){
    delay(1000);
    Serial.print(".");
  }
  Serial.println("Connected");
}

void create_udp(){
  udp.begin(input_port);
  Serial.println("udp created");
}

bool split_request(std::vector<std::string>& output, std::string input, char delim){
  output.clear();
  bool success = false;
  std::string word = "";
  for(char c:input){
    if(c == delim){
      output.push_back(word);
      word = "";
      success = true;
    }else{
      word += c;
    }
  }
  output.push_back(word);
  return success;
}

void readScale(int scaleIndex){
  delay(1); //allows wdt function to reset
  char result[8];
  float weight = SCALE_ARRAY[scaleIndex].get_units();
  dtostrf(weight,6,0,result);
  udp.write(result);
  return;
}

void tareScale(int scaleIndex){
  delay(1);
  SCALE_ARRAY[scaleIndex].tare();
  udp.write("amNow");
  return;
}

// handles startup procedures
void setup(){
  Serial.begin(BAUDRATE); //set baudrate
  ESP.wdtDisable();
  ESP.wdtEnable(1500);

  connect_to_network();
  create_udp();

  // Creating scale objects
  for(int scale_index : SDA_PINS){
    SCALE_ARRAY[scale_index] = HX711(SDA_PINS[scale_index], CLK);
    SCALE_ARRAY[scale_index].set_scale(CAL_ARRAY[scale_index]); // This value is obtained by using the calibraattori sketch
  }
}

void loop(){
  int packet_size = udp.parsePacket();
  if(packet_size){
    
    int length = udp.read(incoming_request,255);
    if (length > 0){
      incoming_request[length] = 0;
    }
    
    // Creating reply message
    udp.beginPacket(udp.remoteIP(), udp.remotePort());

    // When udp_command has multiple parts separated by '_'
    bool success = split_request(udp_command, incoming_request, '_');
    if(success){
      if(udp_command.at(0) == "readScale"){
        readScale(std::stoi(udp_command.at(1)));
      }else if(udp_command.at(0) == "tareScale"){
        tareScale(std::stoi(udp_command.at(1)));
      }
    }
    
    // unknown command
    else {
      udp.write("readDocumentationYouPeaceOfShit");
    }
    udp.endPacket();
  } 
}
