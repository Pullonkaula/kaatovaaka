/*
 Example using the SparkFun HX711 breakout board with n kappaletta vaakojen kanssa :D
 By: Nathan Seidle, Muokattu by Joonas Pinomäki ja Ossi Koski
 SparkFun Electronics
 Date: November 19th, 2014
 License: This code is public domain but you buy me a beer if you use this and we meet someday (Beerware license).
 This example demonstrates basic scale output. See the calibration sketch to get the calibration_factor for your
 specific load cell setup.
 This example code uses bogde's excellent library: https://github.com/bogde/HX711
 bogde's library is released under a GNU GENERAL PUBLIC LICENSE
 The HX711 does one thing well: read load cells. The breakout board is compatible with any wheat-stone bridge
 based load cell which should allow a user to measure everything from a few grams to tens of tons.
 Arduino pin 2 -> HX711 CLK
 3 -> DAT
 5V -> VCC
 GND -> GND
 The HX711 board can be powered from 2.7V to 5V so the Arduino 5V power should be fine.
*/

#include "HX711.h"

// Defines
#define BAUDRATE 115200 //Speed of serial communication in baud.
#define CLK D0 // Serial clock pin for HX711 modules.
int SDA_PINS[] = {D1, D2, D3, D4}; // Serial data pins for HX711 modules, in order.
int CAL_ARRAY[] = {2139, 2139, 2139, 2139}; //These values are obtained using the calibraattori sketch

// Automatic
int n_scales = sizeof(SDA_PINS)/sizeof(SDA_PINS[0]);
HX711 SCALE_ARRAY[] = {};

void setup() {
  Serial.begin(BAUDRATE);

  for(int scale_index : SDA_PINS){
    SCALE_ARRAY[scale_index] = HX711(SDA_PINS[scale_index], CLK);
    SCALE_ARRAY[scale_index].set_scale(CAL_ARRAY[scale_index]); //This value is obtained by using the calibraattori sketch
    SCALE_ARRAY[scale_index].tare(); //Assuming there is no weight on the scale at start up, reset the scale to 0
  }
}

void loop() {
  for(int scale_index = 0; scale_index < n_scales; scale_index++){
    Serial.print("scale: ");
    Serial.print(scale_index);
    Serial.print(" value: ");
    Serial.print(SCALE_ARRAY[scale_index].get_units(), 4); //scale.get_units() returns a float
    Serial.println(" grams"); //You can change this to kg but you'll need to refactor the calibration_factor
  }
  delay(1000);
}
